from vantage6.tools.mock_client import ClientMockProtocol
import vtg_coxph
import numpy as np
import warnings

warnings.filterwarnings("ignore")

# Initialize the mock server. The datasets simulate the local datasets from
# the node. In this case we have two parties having two different datasets:
# data_test.csv and data_test.csv. The module name needs to be the name of your algorithm
# package. This is the name you specified in `setup.py`, in our case that
# would be vtg_coxph.
client = ClientMockProtocol(
    datasets=["./local/data_test.csv", "./local/data_test.csv"],
    module="vtg_coxph"
)

org_ids = ids = [1, 2]



task = client.create_new_task(
    input_={
        'master': 1,
        'method':'master',
        'kwargs': {
            'expl_vars': ['0','1','2','3','4']
            ,'censor_col':'C'
            ,'time_col':'T'
                                }
    },
    organization_ids=[org_ids[0]]
)

results = client.get_results(task.get("id"))
